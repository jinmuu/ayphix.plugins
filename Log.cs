﻿using Ayphix.Plugins;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Plugins {
    public class Log {

        private static Log instance;
        public static Log Instance {
            get {
                if (instance == null) {
                    instance = new Log();
                }
                return instance;
            }
        }

        private string Filename;
        private string InstanceString;

        public Log() {

        }

        public void StartInstance(string file = "log.log") {
            Filename = Environment.CurrentDirectory + "/" + file;
            if (!File.Exists(Environment.CurrentDirectory + "/" + file)) {
                File.Create(Filename).Close();
            }
            InstanceString = RandomEX.RandomString(12) + " On: [" + DateTime.Now.ToShortDateString() + " " + (DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString()) + "]";
            Console.WriteLine("-----------Starting Instance: " + InstanceString + "-----------");
            File.AppendAllText(Filename, "-----------Starting Instance: " + InstanceString + "-----------\n");
        }

        public void EndInstance() {
            Console.WriteLine("------------Ending Instance: " + InstanceString + "------------\n_______________________________________________________________________________\n");
            File.AppendAllText(Filename, "------------Ending Instance: " + InstanceString + "------------\n_______________________________________________________________________________\n");
        }

        public void LogMessage(string message) {
            Console.WriteLine("[" + DateTime.Now.ToShortDateString() + " " + (DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString()) + "]" + message);
            File.AppendAllText(Filename, "[" + DateTime.Now.ToShortDateString() + " " + (DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString()) + "]" + message + "\n");
        }
    }
}
