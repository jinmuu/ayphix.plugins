﻿using Lidgren.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ayphix.Plugins {
    public class Client {
         NetPeerConfiguration NetConfig;

         NetClient NetClient;

         NetOutgoingMessage Message;

         Server Server;

        public  string ClientName;

        public  Dictionary<string, NetworkUpdate> Callers = new Dictionary<string, NetworkUpdate>();

        public Client() {

        }

        public  void Init() {
            NetConfig = new NetPeerConfiguration(ClientName);

            NetClient = new NetClient(NetConfig);

            NetClient.Start();
        }

        public  void Start() {
            
        }

        public  void Connection(string ip, int port) {
            NetClient.Connect(ip, port);
        }

        public  void Disconnect() {
            NetClient.Disconnect("");
        }

        public  void AttachServer(Server server) {
            Server = server;
        }

        public  void Update() {
            NetIncomingMessage msg;
            while ((msg = NetClient.ReadMessage()) != null) {
                switch (msg.MessageType) {
                    case NetIncomingMessageType.WarningMessage:
                        Console.WriteLine(msg.ReadString());
                        break;
                    case NetIncomingMessageType.ErrorMessage:
                        Console.WriteLine(msg.ReadString());
                        break;
                    case NetIncomingMessageType.Data:
                        string original = msg.ReadString();
                        Callers[original].Update(msg);
                        break;
                    default:
                        Console.WriteLine("Unhandled message type\n");
                        break;
                }
                NetClient.Recycle(msg);
            }
        }

        public  NetConnectionStatus GetStatus() {
            return NetClient.ConnectionStatus;
        }

        public  void RegisterCaller(string name, NetworkUpdate updater) {
            Callers.Add(name, updater);
        }

        public  void UnhookCaller(string name) {
            Callers.Remove(name);
        }

        public  void CreateMessage() {
            Message = NetClient.CreateMessage();
        }

        public  void AddMessage(string msg) {
            Message.Write(msg);
        }

        public  void AddMessage(bool msg) {
            Message.Write(msg);
        }

        public  void AddMessage(int msg) {
            Message.Write(msg);
        }

        public  void AddMessage(float msg) {
            Message.Write(msg);
        }

        public  void AddMessage(byte msg) {
            Message.Write(msg);
        }

        public void AddMessage(object obj) {
            Message.WriteAllFields(obj);
        }

        public void AddMessage(byte[] msg, int offset, int length) {
            Message.Write(msg, offset, length);
        }

        public  void SendMessage() {
            NetClient.SendMessage(Message, NetDeliveryMethod.ReliableOrdered);
        }
    }
}
