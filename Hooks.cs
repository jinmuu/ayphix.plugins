﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ayphix.Plugins {
    public static class Hooks {
        public static Dictionary<string, string> _Hooks = new Dictionary<string, string>();

        public static void AddHook(string key, string hook) {
            _Hooks.Add(key, hook);
        }
    }
}
