﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lidgren.Network;
using rScripting;
using System.Threading;

namespace Ayphix.Plugins {
    public class LidgrenServer : AbstractServer {

        NetPeerConfiguration NetConfig;
        NetServer NetServer;

        NetIncomingMessage MessageIn;
        NetOutgoingMessage Message;

        AbstractClient Client;

        public override void Init() {
            NetConfig = new NetPeerConfiguration(ServerName);
            NetConfig.Port = ServerPort;

            NetServer = new NetServer(NetConfig);
        }

        public override void Start() {
            NetServer.Start();
        }

        public override void CreateMessage() {
            Message = NetServer.CreateMessage();
        }

        public override void AddMessage(string msg) {
            Message.Write(msg);
        }

        public override void AddMessage(bool msg) {
            Message.Write(msg);
        }

        public override void AddMessage(byte msg) {
            Message.Write(msg);
        }

        public override void AddMessage(float msg) {
            Message.Write(msg);
        }

        public override void AddMessage(byte[] msg, int offset, int length) {
            Message.Write(msg, offset, length);
        }

        public override void AddMessage(int msg) {
            Message.Write(msg);
        }

        public override void AddMessage(object obj) {
            Message.WriteAllFields(obj);
        }

        public override void AttachClient(AbstractClient client) {
            Client = client;
        }

        public override void SendMessage(object conn) {
            NetServer.SendMessage(Message, (NetConnection)conn, NetDeliveryMethod.ReliableOrdered);
        }

        public override void SendMessageToAll() {
            NetServer.SendToAll(Message, NetDeliveryMethod.ReliableOrdered);
        }

        public override void Update() {
            NetIncomingMessage msg;
            while ((msg = NetServer.ReadMessage()) != null) {
                switch (msg.MessageType) {
                    case NetIncomingMessageType.WarningMessage:
                        Console.WriteLine(msg.ReadString());
                        break;
                    case NetIncomingMessageType.ErrorMessage:
                        Console.WriteLine(msg.ReadString());
                        break;
                    case NetIncomingMessageType.Data:
                        if (!Parent.HardCoded) {
                            string original = msg.ReadString();
                            string[] command = original.Split('|');
                            ScriptFactory f = new ScriptFactory(_Engine.CompiledAssembly);
                            ScriptObject obj = f.GetScript(command[0]);
                            obj.InvokeMethod(command[1], msg);
                        } else {
                            if (Parent.IncomingMessage != null) {
                                Parent.IncomingMessage.Invoke(msg);
                            }
                        }
                        break;
                    case NetIncomingMessageType.StatusChanged:
                        NetConnectionStatus status = (NetConnectionStatus)msg.ReadByte();
                        switch (status) {
                            case NetConnectionStatus.Connected:
                                if (Parent.OnConnect != null) {
                                    Parent.OnConnect.Invoke(msg);
                                }
                                break;
                            case NetConnectionStatus.Disconnected:
                                if (Parent.OnDisconnect != null) {
                                    Parent.OnDisconnect.Invoke(msg);
                                }
                                break;
                        }
                        break;
                    default:
                        Console.WriteLine("Unhandled message type\n");
                        break;
                }
                NetServer.Recycle(msg);
            }
        }

    }
}
