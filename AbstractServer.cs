﻿using Lidgren.Network;
using rScripting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ayphix.Plugins {
    public class AbstractServer {

        public string ServerName;
        public int ServerPort;

        public CompileEngine _Engine = new CompileEngine();
        public CompileEngine _GlobalEngine = new CompileEngine();

        public ScriptFactory GlobalFactory;

        public Server Parent;

        public bool CompiledGlobals = false;
        public bool MaintenanceMode = false;
        public bool HasGlobals = false;

        public virtual void Init() { }
        public virtual void Start() { }
        public virtual void AttachClient(AbstractClient client) { }
        public virtual void Update() { }

        public void CompileScripts(string dir, string ext) {
            try {
                if (!CompiledGlobals || MaintenanceMode) {
                    _GlobalEngine.AddAssemblyReference("Lidgren.Network.dll");
                    _GlobalEngine.AddAssemblyReference("Plugins.dll");
                    bool errors_global = _GlobalEngine.Compile(dir + "\\Global");
                    if (!errors_global) {
                        Console.WriteLine(_GlobalEngine.Errors);
                        Console.WriteLine("Press any key to quit.");
                        return;
                    }
                    Console.WriteLine("Globals Compiled");
                    CompiledGlobals = true;
                    HasGlobals = true;
                }
                _Engine.AddAssemblyReference("Lidgren.Network.dll");
                _Engine.AddAssemblyReference("Plugins.dll");
                if (CompiledGlobals && HasGlobals) {
                    //_Engine.AddAssemblyReference(_GlobalEngine.CompiledAssembly.GetSatelliteAssembly(System.Globalization.CultureInfo.CurrentCulture));
                }
                /*_Engine.AddAssemblyReference("MySql.Data.dll");
                 _Engine.AddAssemblyReference("MySql.Web.dll");*/

                bool NoErrors = _Engine.Compile(dir + "\\Virtual");
                if (!NoErrors) {
                    Console.WriteLine(_Engine.Errors);
                    Console.WriteLine("Press any key to exit");
                    /*Console.ReadLine();
                    Environment.Exit(1);*/
                    return;
                }
                //Console.Clear();
                Console.WriteLine("Virtuals Compiled");
            } catch (Exception e) {
                Console.WriteLine(e.Message);
                this.CompileScripts(@"./Scripts", ".cs");
            }
        }

        public virtual void CreateMessage() { }

        public virtual void AddMessage(string msg) { }

        public virtual void AddMessage(bool msg) { }

        public virtual void AddMessage(int msg) { }

        public virtual void AddMessage(float msg) { }

        public virtual void AddMessage(byte msg) { }

        public virtual void AddMessage(byte[] msg, int offset, int length) { }

        public virtual void AddMessage(object obj) { }

        public virtual void SendMessage(object conn) { }

        public virtual void SendMessageToAll() { }
    }
}
