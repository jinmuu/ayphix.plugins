﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using Plugins;

namespace Ayphix.Plugins {
    public class DB {
        #region ActiveRecord Members
        private List<string> select = new List<string>();
        private Dictionary<string, string> where = new Dictionary<string, string>();
        private string order_by = "";
        private string limit = "";
        private string from = "";
        private Dictionary<string, string> like = new Dictionary<string, string>();
        private DBResult res;
        #endregion

        #region Initialization
        private Dictionary<string, string> Settings = new Dictionary<string, string>();
        private string Host;
        private string Database;
        private string Username;
        private string Password;
        private string Prefix;
        private int Port;
        private MySqlConnection dbCon2;
        public DB(Dictionary<string, string> settings) {
            Settings = settings;
            Host = settings["Host"];
            Database = settings["Database"];
            Username = settings["Username"];
            Password = settings["Password"];
            Prefix = settings["Prefix"];
            if (!Int32.TryParse(Settings["Port"], out Port)) {
                Console.WriteLine("The port provided was not a number. In Databases.ini for database: " + settings["Database"]);
            }

            Console.WriteLine("Trying initial connection to Database: " + Database);
            try {
                using (MySqlConnection dbCon2 = new MySqlConnection(ConnectionString())) {
                    dbCon2.Open();

                    dbCon2.Close();
                }
            } catch (MySql.Data.MySqlClient.MySqlException e) {
                Console.WriteLine("Connection to Database: " + Database + " has failed, see below for the details.");
                Console.WriteLine(e.Message);
                return;
            }
            Console.WriteLine("Successfully Connected to Database: " + Database);
        }

        public DB(string host, string data, string username, string password, string prefix, int port) {
            Settings.Add("Host", host);
            Settings.Add("Database", data);
            Settings.Add("Username", username);
            Settings.Add("Password", password);
            Settings.Add("Prefix", prefix);
            Settings.Add("Port", port + "");
            Host = host;
            Database = data;
            Username = username;
            Password = password;
            Prefix = prefix;
            Port = port;

            Console.WriteLine("Trying initial connection to Database: " + Database);
            try {
                dbCon2 = new MySqlConnection(ConnectionString());
                using(dbCon2) {
                    dbCon2.Open();

                    dbCon2.Close();
                }
            } catch (MySql.Data.MySqlClient.MySqlException e) {
                Console.WriteLine("Connection to Database: " + Database + " has failed, see below for the details.");
                Console.WriteLine(e.Message);
                return;
            }
            Console.WriteLine("Successfully Connected to Database: " + Database);
        }

        public string ConnectionString() {
            return "server=" + Host + ";user id=" + Username + ";password=" + Password + ";database=" + Database + ";ConvertZeroDateTime=true;Allow Zero Datetime=true;Pooling=true";
        }
        #endregion

        #region ActiveRecord Methods
        public void Insert(string table, Dictionary<string, string> values) {
            string query = "INSERT INTO `" + Prefix + table + "` ";
            List<string> keys = new List<string>();
            List<string> vals = new List<string>();
            foreach (KeyValuePair<string, string> ins in values) {
                keys.Add(ins.Key);
                vals.Add(ins.Value);
            }
            query += "(";
            foreach (string key in keys) {
                query += "`" + key + "`,";
            }
            query = query.TrimEnd(',');
            query += ") VALUES (";
            foreach (string val in vals) {
                query += "'" + MySqlHelper.EscapeString(val) + "',";
            }
            query = query.TrimEnd(',');
            query += ");";
            Console.WriteLine(query);
            using (MySqlConnection dbCon2 = new MySqlConnection(ConnectionString())) {
                dbCon2.Open();
                MySqlCommand cmd = new MySqlCommand(query, dbCon2);
                cmd.ExecuteNonQuery();
                dbCon2.Close();
            }
        }

        public DB Select(string select) {
            this.select.Add(select);
            return this;
        }

        public DB Select(List<string> select) {
            foreach (string sel in select) {
                this.select.Add(sel);
            }
            return this;
        } // i think this methods are redundant. When you need to make some advanced SQL request with GROUP BY, MINUS, UNION, etd its 

        public DB Where(Dictionary<string, string> whr) {
            foreach (KeyValuePair<string, string> where in whr) {
                this.where.Add(where.Key, where.Value);
            }
            return this;
        }

        public DB Where(string key, string value) {
            if (!this.where.ContainsKey(key)) {
                this.where.Add(key, value);
            }
            return this;
        }

        public DB Like(Dictionary<string, string> like) {
            foreach (KeyValuePair<string, string> lik in like) {
                this.like.Add(lik.Key, lik.Value);
            }
            return this;
        }

        public DB Like(string key, string value) {
            this.like.Add(key, value);
            return this;
        }

        public DB From(string from) {
            this.from = from;
            return this;
        }

        public DB OrderBy(string order, string patt) {
            order_by = order + " " + patt;
            return this;
        }

        public DB Limit(string limit) {
            this.limit = limit;
            return this;
        }

        public DB Delete() {
            string query = "DELETE FROM `" + this.from + "`";
            if (where.Count > 0) {
                query += " WHERE ";

                foreach (KeyValuePair<string, string> whr in where) {
                    query += "`" + whr.Key + "`" + " = '" + whr.Value + "' AND ";
                }

                query = query.Substring(0, query.Length - 5);
            }
            query += ";";
            Console.WriteLine(query);

            using (MySqlConnection dbCon2 = new MySqlConnection(ConnectionString())) {
                dbCon2.Open();
                MySqlCommand cmd = new MySqlCommand(query, dbCon2);
                cmd.ExecuteNonQuery();
                dbCon2.Close();
            }
            return this;
        }

        public DBResult Get() {
            string query = "SELECT ";
            query += select[0];
            query += " FROM `" + Prefix + from + "`";
            if (where.Count > 0) {
                query += " WHERE ";

                foreach (KeyValuePair<string, string> whr in where) {
                    string selector = "=";
                    if (whr.Value.Contains('>') || whr.Value.Contains('<') || whr.Value.Contains("!=")) {
                        selector = "";
                    }
                    query += "`" + whr.Key + "`" + selector + " '" + whr.Value + "' AND ";
                }

                foreach (KeyValuePair<string, string> lik in like) {
                    query += "`" + lik.Key + "`" + " LIKE '" + lik.Value + "' AND ";
                }

                query = query.Substring(0, query.Length - 4);
            }
            if (order_by != "") {
                query += " ORDER BY " + order_by;
            }
            if (limit != "") {
                query += " LIMIT " + limit;
            }
            query = query.TrimEnd(' ');
            query += ";";
            Console.WriteLine(query);
            try {
                using (MySqlConnection dbCon2 = new MySqlConnection(ConnectionString())) {
                    dbCon2.Open();
                    MySqlCommand cmd = new MySqlCommand(query, dbCon2);
                    MySqlDataReader reader = cmd.ExecuteReader();
                    res = new DBResult();
                    int r = 0;
                    while (reader.Read()) {
                        res.rows.Add(new Row());
                        for (int i = 0; i < reader.FieldCount; ++i) {
                            res.rows[r].Add(reader.GetName(i), reader.GetString(i));
                        }
                        ++r;
                    }
                    res.CountStats();
                    reader.Close();
                    dbCon2.Close();
                }
            } catch (MySql.Data.MySqlClient.MySqlException e) {
                Console.WriteLine(e.Message);
            }
            select.Clear();
            where.Clear();
            like.Clear();
            order_by = "";
            limit = "";
            return res;
        }
        #endregion

        #region Query
        public DBResult Query(string query, params object[] args) {
            query = Format(query, args);
            //Console.WriteLine(query);
            if (query.Split(' ')[0] == "INSERT") {
                try {
                    using (dbCon2) {
                        dbCon2.Open();
                        MySqlCommand cmd = new MySqlCommand(query, dbCon2);
                        cmd.ExecuteNonQuery();
                        dbCon2.Close();
                    }
                } catch (MySql.Data.MySqlClient.MySqlException e) {
                    Console.WriteLine(e.Message);
                }
                return null;
            } else if (query.Split(' ')[0] == "UPDATE") {
                try {
                    using (dbCon2) {
                        dbCon2.Open();
                        MySqlCommand cmd = new MySqlCommand(query, dbCon2);
                        cmd.ExecuteNonQuery();
                        dbCon2.Close();
                    }
                } catch (MySql.Data.MySqlClient.MySqlException e) {
                    Console.WriteLine(e.Message);
                }
                return null;
            } else if (query.Split(' ')[0] == "DELETE") {
                try {
                    using (dbCon2) {
                        dbCon2.Open();
                        MySqlCommand cmd = new MySqlCommand(query, dbCon2);
                        cmd.ExecuteNonQuery();
                        dbCon2.Close();
                    }
                } catch (MySql.Data.MySqlClient.MySqlException e) {
                    Console.WriteLine(e.Message);
                }
                return null;
            } else {
                DBResult res = new DBResult();
                try {
                    using (dbCon2) {
                        dbCon2.Open();
                        MySqlCommand cmd = new MySqlCommand(query, dbCon2);
                        MySqlDataReader reader = cmd.ExecuteReader();
                        res = new DBResult();
                        int r = 0;
                        while (reader.Read()) {
                            res.rows.Add(new Row());
                            for (int i = 0; i < reader.FieldCount; ++i) {
                                res.rows[r].Add(reader.GetName(i), reader.GetValue(i));
                            }
                            ++r;
                        }
                        res.CountStats();
                        reader.Close();
                        dbCon2.Close();
                    }
                } catch (MySql.Data.MySqlClient.MySqlException e) {
                    Console.WriteLine(e.Message);
                }
                return res;
            }
        }

        public DB Thread() {
            return new DB(this.Host, this.Database, this.Username, this.Password, this.Prefix, this.Port);
        }

        #endregion

        string Format(string str, object[] args) {
            for (int i = 0; i< args.Length; i++) {
                args[i] = "'" + args[i] + "'";
            }

            return string.Format(str, args);
        }
    }

    public class DBResult : IEnumerable<Row> {

        public List<Row> rows = new List<Row>();

        public int NumRows;

        public void CountStats() {
            NumRows = rows.Count;
        }

        public List<dynamic> Result() {
            if (NumRows > 0) {
                var res_obj = new ExpandoObject() as IDictionary<string, object>;
                List<dynamic> rows = new List<dynamic>();
                int count = 0;
                foreach (Dictionary<string, string> row in rows) {
                    foreach (KeyValuePair<string, string> field in row) {
                        rows.Add(new ExpandoObject() as IDictionary<string, object>);
                        rows[count].Add(field.Key, field.Value);
                    }
                    ++count;
                }
                return rows;
            } else {
                return null;
            }
        }

        public List<Row> Result_Array() {
            return rows;
        }

       /* public IEnumerator GetEnumerator() {
            return rows.GetEnumerator();
        }*/

        public IEnumerator<Row> GetEnumerator() {
            return rows.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator() {
            return rows.GetEnumerator();
        }
    }

    public class Row : Dictionary<string, object> {
        public Row() {

        }
    }

    public static class DBHost {
        public static Dictionary<string, DB> Databases = new Dictionary<string, DB>();

        public static void LoadDatabases() {
            foreach (KeyValuePair<string, Dictionary<string, string>> db in Config.ConfigData["Databases"]) {
                Databases.Add(db.Key, new DB(db.Value));
            }
        }

        public static void AddDatabase(string db, string host, string data, string username, string password, string prefix, int port) {
            Databases.Add(db, new DB(host,data,username,password,prefix,port));
        }
    }
}
