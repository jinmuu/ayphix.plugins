﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using rScripting;
using System.IO;
using Lidgren.Network;

namespace Ayphix.Plugins {
    public class Server {
        const float ClientVersion = 1.0f;

        AbstractClient Client;

        public AbstractServer NetServer;

        public delegate void OnMsg(NetIncomingMessage msg);
        public OnMsg IncomingMessage;

        public delegate void OnDisc(NetIncomingMessage conn);
        public OnDisc OnDisconnect;

        public delegate void OnConn(NetIncomingMessage conn);
        public OnConn OnConnect;


        public string ServerName;
        public int ServerPort;
        public bool HardCoded = false;

        public enum ServerType {
            Lidgren,
            SFML
        }

        public void Init(ServerType driver) {
            switch(driver) {
                case ServerType.SFML:
                    NetServer = new SFMLServer();
                    break;
                case ServerType.Lidgren:
                    NetServer = new LidgrenServer();
                    NetServer.Parent = this;
                    break;
            }
            NetServer.ServerName = ServerName;
            NetServer.ServerPort = ServerPort;
            NetServer.Init();
        }

        public void Start() {
            NetServer.Start();
            Console.WriteLine("Server start on port " + NetServer.ServerPort + " with the name of " + NetServer.ServerName);
        }

        public void AttachClient(AbstractClient client) {
            Client = client;
        }

        public void Update() {
            NetServer.Update();
        }

        public void CreateMessage() {
            NetServer.CreateMessage();
        }

        public void AddMessage(string msg) {
            NetServer.AddMessage(msg);
        }

        public void AddMessage(bool msg) {
            NetServer.AddMessage(msg);
        }

        public void AddMessage(int msg) {
            NetServer.AddMessage(msg);
        }

        public void AddMessage(float msg) {
            NetServer.AddMessage(msg);
        }

        public void AddMessage(byte msg) {
            NetServer.AddMessage(msg);
        }

        public void AddMessage(byte[] msg, int offset, int length) {
            NetServer.AddMessage(msg, offset, length);
        }

        public void AddMessage(object obj) {
            NetServer.AddMessage(obj);
        }

        public void SendMessage(object conn) {
            NetServer.SendMessage(conn);
        }

        public void SendMessageToAll() {
            NetServer.SendMessageToAll();
        }
    }
}
