﻿using IniParser;
using IniParser.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plugins {
    public static class Config {
        public static Dictionary<string, Dictionary<string, Dictionary<string, string>>> ConfigData = new Dictionary<string, Dictionary<string, Dictionary<string, string>>>();

        public static bool LoadFile(string file) {
            if (File.Exists(file)) {
                FileIniDataParser conf = new FileIniDataParser();
                IniData data = conf.ReadFile(file);
                string filename = Path.GetFileNameWithoutExtension(file);
                ConfigData.Add(filename, new Dictionary<string, Dictionary<string, string>>());
                foreach (SectionData group in data.Sections) {
                    ConfigData[filename].Add(group.SectionName, new Dictionary<string, string>());
                    foreach (KeyData keys in group.Keys) {
                        ConfigData[filename][group.SectionName].Add(keys.KeyName, keys.Value);
                    }
                }
                return true;
            } else {
                return false;
            }
        }

        public static void SaveFile(string file) {
            if (!File.Exists(file)) {
                File.Create(file).Close();
            }
            if (ConfigData.ContainsKey(Path.GetFileNameWithoutExtension(file))) {
                IniData data = new IniData();
                foreach (KeyValuePair<string, Dictionary<string, string>> sections in ConfigData[Path.GetFileNameWithoutExtension(file)]) {
                    data.Sections.AddSection(sections.Key);
                    foreach (KeyValuePair<string, string> configs in sections.Value) {
                        data.Sections[sections.Key].AddKey(configs.Key, configs.Key);
                    }
                }
                FileIniDataParser conf = new FileIniDataParser();
                using (StreamWriter writer = new StreamWriter(file)) {
                    conf.WriteData(writer, data);
                    writer.Close();
                }
            }
        }
    }
}
