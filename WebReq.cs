﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Plugins {
    public class WebReq {
        private string strresponse = "";

        public WebReq(string url, string data, bool get = false) {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url + ((get) ? "?" + data : ""));
            if (!get) {
                request.Method = "POST";
            } else {
                request.Method = "GET";
            }

            request.Proxy = null;

            #region Hidden Key
            data += "&CODE=1l7p6wDYqC78rLLvfA02";
            #endregion
            if (request.Method == "POST") {
                byte[] array = Encoding.UTF8.GetBytes(data);
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = data.Length;
                Stream datastream = request.GetRequestStream();
                datastream.Write(array, 0, data.Length);
                datastream.Close();
                WebResponse response = request.GetResponse();
                datastream = response.GetResponseStream();
                StreamReader reader = new StreamReader(datastream);
                strresponse = HttpUtility.UrlDecode(reader.ReadToEnd());
                datastream.Close();
                reader.Close();
                response.Close();
            } else {
                request.ContentType = "text/xml; encoding='utf-8'";
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream());
                strresponse = HttpUtility.UrlDecode(reader.ReadToEnd());
                response.Close();
                reader.Close();
            }
        }

        public string GetResponse() {
            return strresponse;
        }
    }
}
