﻿using Lidgren.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Plugins {
    public enum AccountType {
        Member = 12,
        GMod = 18,
        Mod = -1,
        SRVet = 9,
        Admin = 15
    }
    public class User {
        public int ID;
        public string Username;
        public string Nickname;
        public string IP;
        public string Avatar;
        public AccountType Type;
        public List<string> Friends = new List<string>();
        public int Group;
        public NetConnection Connection;
    }

    public static class UserManager {
        private static Dictionary<string, User> Users1 = new Dictionary<string, User>();
        private static Dictionary<string, User> Users2 = new Dictionary<string, User>();

        public static bool AddUser(string username, NetConnection connection) {
            if (CheckUser(username, connection)) {
                Users1.Add(username, new User() { ID = Users1.Count, Username = username, Nickname = username, Connection = connection, IP = connection.RemoteEndPoint.Address.ToString() + connection.RemoteEndPoint.Port.ToString() });
                Users2.Add(connection.RemoteEndPoint.Address.ToString() + connection.RemoteEndPoint.Port.ToString(), new User() { ID = Users1.Count, Username = username, Nickname = username, Connection = connection, IP = connection.RemoteEndPoint.Address.ToString() + connection.RemoteEndPoint.Port.ToString() });
                Console.WriteLine("User added with username: " + username + " and the IP of " + connection.RemoteEndPoint.Address.ToString() + connection.RemoteEndPoint.Port.ToString());
                return true;
            } else {
                return false;
            }
        }

        public static bool AddUser(User user) {
            if (CheckUser(user.Username, user.Connection)) {
                user.ID = Users1.Count;
                Users1.Add(user.Username, user);
                Users2.Add(user.IP, user);
                return true;
            } else {
                return false;
            }
        }

        public static string FormatIP(NetConnection connection) {
            return connection.RemoteEndPoint.Address.ToString() + connection.RemoteEndPoint.Port.ToString();
        }

        public static bool CheckUser(string username, NetConnection connection) {
            if (!Users1.ContainsKey(username) && !Users2.ContainsKey(connection.RemoteEndPoint.Address.ToString() + connection.RemoteEndPoint.Port.ToString())) {
                return true;
            } else {
                return false;
            }
        }

        public static bool RemoveUser(string username, NetConnection connection) {
            if (!CheckUser(username, connection)) {
                Users1.Remove(username);
                Users2.Remove(connection.RemoteEndPoint.Address.ToString() + connection.RemoteEndPoint.Port.ToString());
                Console.WriteLine("User removed with username: " + username + " and the IP of " + connection.RemoteEndPoint.Address.ToString() + connection.RemoteEndPoint.Port.ToString());
                return true;
            } else {
                Console.WriteLine("Could not find user");
                return false;
            }
        }

        public static User GetUser(string username) {
            if (Users1.ContainsKey(username)) {
                return Users1[username];
            } else {
                return null;
            }
        }

        public static User GetUser(NetConnection connection) {
            if (Users2.ContainsKey(connection.RemoteEndPoint.Address.ToString() + connection.RemoteEndPoint.Port.ToString())) {
                return Users2[connection.RemoteEndPoint.Address.ToString() + connection.RemoteEndPoint.Port.ToString()];
            } else {
                return null;
            }
        }
    }
}
