﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Plugins {
    public class KinXMLNode {
        public KinXMLNode(string name) {
            this.Name = name;
        }
        public string Name { get; set; }
        public string Text { get; set; }
        public List<string> Comments = new List<string>();
        public string CData { get; set; }
        public KinXMLNode Parent;
        public Dictionary<string, string> Attributes = new Dictionary<string, string>();
        public Dictionary<string, List<KinXMLNode>> Children = new Dictionary<string, List<KinXMLNode>>();

        public int TextToInt() {
            return Int32.Parse(Text);
        }

        public float TextToFloat() {
            return Single.Parse(Text);
        }

        public bool TextToBoolean() {
            return Boolean.Parse(Text);
        }

        public int AttributeToInt(string key) {
            if (Attributes.ContainsKey(key)) {
                return Int32.Parse(Attributes[key]);
            } else {
                return -1;
            }
        }

        public float AttributeToFloat(string key) {
            if (Attributes.ContainsKey(key)) {
                return Single.Parse(Attributes[key]);
            } else {
                return -1;
            }
        }

        public bool AttributeToBoolean(string key) {
            if (Attributes.ContainsKey(key)) {
                return Boolean.Parse(Attributes[key]);
            } else {
                return false;
            }
        }
    }
    public class KinXML : Dictionary<string, List<KinXMLNode>> {
        public KinXML(string file, bool isSring = false) {
            List<KinXMLNode> OpenNodes = new List<KinXMLNode>();
            XmlReader reader = null;
            if (isSring) {
                reader = XmlReader.Create(new StringReader(file), null);
            } else {
                reader = XmlReader.Create(file);
            }
            using (reader) {
                while (reader.Read()) {
                    switch (reader.NodeType) {
                        case XmlNodeType.Element:
                            KinXMLNode node = new KinXMLNode(reader.Name);
                            if (OpenNodes.Count > 0) {
                                if (reader.HasAttributes) {
                                    while (reader.MoveToNextAttribute()) {
                                        node.Attributes.Add(reader.Name, reader.Value);
                                    }
                                }
                                reader.MoveToElement();
                                node.Parent = OpenNodes[OpenNodes.Count - 1];
                                if (!node.Parent.Children.ContainsKey(node.Name)) {
                                    node.Parent.Children.Add(node.Name, new List<KinXMLNode>());
                                }
                                node.Parent.Children[node.Name].Add(node);
                            } else {
                                if (reader.HasAttributes) {
                                    while (reader.MoveToNextAttribute()) {
                                        node.Attributes.Add(reader.Name, reader.Value);
                                    }
                                }
                                reader.MoveToElement();
                                if (!this.ContainsKey(node.Name)) {
                                    this.Add(node.Name, new List<KinXMLNode>());
                                }
                                this[node.Name].Add(node);
                                //Console.WriteLine("Adding node " + node.Name);
                            }
                            if (!reader.IsEmptyElement) {
                                OpenNodes.Add(node);
                            }
                            break;
                        case XmlNodeType.EndElement:
                            if (!reader.IsEmptyElement) {
                                if (OpenNodes.Count > 0) {
                                    OpenNodes.RemoveAt(OpenNodes.Count - 1);
                                }
                            }
                            break;
                        case XmlNodeType.Comment:
                            if (OpenNodes.Count > 0) {
                                KinXMLNode OpenNode = OpenNodes[OpenNodes.Count - 1];
                                OpenNode.Comments.Add(reader.Value.Trim());
                            }
                            break;
                        case XmlNodeType.CDATA:
                            if (OpenNodes.Count > 0) {
                                KinXMLNode OpenNode = OpenNodes[OpenNodes.Count - 1];
                                OpenNode.CData = reader.Value.Trim();
                            }
                            break;
                        case XmlNodeType.Text:
                            if (OpenNodes.Count > 0) {
                                KinXMLNode OpenNode = OpenNodes[OpenNodes.Count - 1];
                                OpenNode.Text = reader.Value.Trim();
                            }
                            break;
                    }
                }
            }
        }
    }
}
